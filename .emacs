;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; emacs     config ;;;;
;; what am I even doing ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;

;; load custom variables in another file
(setq custom-file (concat user-emacs-directory "/custom.el"))

;; MELPA stuff
(require 'package)
(package-initialize)
(setq package-archives
      '(("gnu" . "https://elpa.gnu.org/packages/")
	("marmalade" . "https://marmalade-repo.org/packages/")
	("melpa" . "http://melpa.org/packages/")))

;; enable show paren mode
(show-paren-mode t)
(setq show-paren-delay 0)

;; electric pair mode: automatic paren/bracket pairing
(electric-pair-mode t)

;; UI customizations
(menu-bar-mode -1)
(fringe-mode 0)
(tool-bar-mode -1)
(toggle-scroll-bar -1)



;; ido mode (automatically show completions)
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)

;; shortcuts for moving btw windows
;; <left> = left arrow, etc. if you want to use arrows
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)

;; when using ansi-term, use bash shell
;; because zsh and fish colors mess up the display
;; (setq-default explicit-shell-file-name "/run/current-system/sw/bin/bash")

;; make a shortcut for customize-themes
(defalias 'ct 'customize-themes)

;; add this path to theme load path
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
;; for packages
(add-to-list 'load-path "~/.emacs.d/elisp")
    
  ;; org-mode settings
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-font-lock-mode 1)

;; put backup files in a new place
(setq backup-directory-alist `(("." . "~/.saves")))
(setq backup-by-copying t)

(put 'erase-buffer 'disabled nil)

  

(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
;; add a keybinding for shell-mode to enable fast clearing
(add-hook 'shell-mode-hook
	  '(lambda ()
	     (local-set-key "\C-l" 'erase-buffer)))
;; add one in CIDER mode to do CIDER's special clear command
;; (add-hook 'cider-repl-mode-hook
;; 	    '(lambda () (define-key cider-repl-mode-map (kbd "C-l")
;; 			  'cider-repl-clear-buffer)))

;; exclude certain buffers from C-x b options
(setq ido-ignore-buffers '("*scratch*" "*Messages*" "*Completions*" "*Echo Area 0*" "*Echo Area 1*" "*code-conversion-work*" "*Minibuf-0*" "*Minibuf-1*"))
;; don't prompt when killing running processes
(setq kill-buffer-query-functions (delq 'process-kill-buffer-query-function kill-buffer-query-functions))
(add-to-list 'default-frame-alist '(font . "Code New Roman 10"))
(set-face-attribute 'default t :font "Code New Roman 10")
(setq-default cursor-type 'box)
(set-cursor-color "#555")
;; don't ask for prompt when killing buffers
(defun kill-this-buffer-safe ()
	"Kill the current buffer"
	(interactive)
	(kill-buffer (current-buffer)))

(global-set-key (kbd "C-x k") 'kill-this-buffer-safe)

(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(setq inhibit-read-only t)
;; lisp stuff (haha this whole file is lisp isn't it)
(load (expand-file-name "~/quicklisp/slime-helper.el"))
(setq inferior-lisp-program "sbcl")
(add-to-list 'load-path "~/.emacs.d/slime")
(require 'slime-autoloads)
(add-to-list 'slime-contribs 'slime-fancy)
(setq slime-net-coding-system 'utf-8-unix)


;; indent using spaces rather than tbas
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)

;; customize the mode-line
;; %-constructs are documented here:
;; https://www.gnu.org/software/emacs/manual/
;; '-> html_node/elisp/_0025_002dConstructs.html
(setq mode-line-end-spaces "*")
;; (setq-default mode-line-format "%&%&%&《%b》《%l:%C》《%m》%-")
(setq-default mode-line-format "%&%&%&<%b><%l:%C><%m>%-")

;; make some abbrevs! mostly to kludge in pinyin input
;; note: type C-q before something in order to *not* abbrev it
(clear-abbrev-table global-abbrev-table)
(define-abbrev-table 'global-abbrev-table
  '( 
    ("ue5" "ü")
    ("UE5" "Ü")
    ("ue1" "ǖ")
    ("UE1" "Ǖ")
    ("ue2" "ǘ")
    ("UE2" "Ǘ")
    ("ue3" "ǚ")
    ("UE3" "Ǚ")
    ("ue4" "ù")
    ("UE4" "Ǜ")
    
    ("a1" "ā")
    ("A1" "Ā")
    ("a2" "á")
    ("A2" "Á")
    ("a3" "ǎ")
    ("A3" "Ǎ")
    ("a4" "à")
    ("A4" "À")
    
    ("e1" "ē")
    ("E1" "Ē")
    ("e2" "é")
    ("E2" "É")
    ("e3" "ě")
    ("E3" "Ě")
    ("e4" "è")
    ("E4" "È")
    
    ("i1" "ī")
    ("I1" "Ī")
    ("i2" "í")
    ("I2" "Í")
    ("i3" "ǐ")
    ("I3" "Ǐ")
    ("i4" "ì")
    ("I4" "Ì")

    ("o1" "ō")
    ("O1" "Ō")
    ("o2" "ó")
    ("O2" "Ó")
    ("o3" "ǒ")
    ("O3" "Ǒ")
    ("o4" "ò")
    ("O4" "Ò")

    ("u1" "ū")
    ("U1" "Ū")
    ("u2" "ú")
    ("U2" "Ú")
    ("u3" "ǔ")
    ("U3" "Ǔ")
    ("u4" "ù")
    ("U4" "Ù")

    ("sqgl" "﹏")
    ))

(set-default 'abbrev-mode t)
(setq save-abbrevs nil)

;; theme cycler!
(setq my-themes '(visions purple-haze))
(setq my-theme-index 0)

(defun my-cycle-theme ()
  (interactive)
  (setq my-theme-index (% (1+ my-theme-index) (length my-themes)))
  (my-load-theme))

(defun my-load-theme ()
  (my-try-load-theme (nth my-theme-index my-themes)))

(defun my-try-load-theme (theme)
  (if (ignore-errors (load-theme theme :no-confirm))
      (mapcar #'disable-theme (remove theme custom-enabled-themes))
    (message "Unable to file theme file for `%s'" theme)))

(defalias 'tt 'my-cycle-theme)


;; load custom variables
(load-file "~/.emacs.d/custom.el")
